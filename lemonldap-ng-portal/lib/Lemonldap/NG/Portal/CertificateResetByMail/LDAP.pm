package Lemonldap::NG::Portal::CertificateResetByMail::LDAP;

use strict;
use Mouse;
use Lemonldap::NG::Portal::Main::Constants qw(
  PE_LDAPCONNECTFAILED
  PE_LDAPERROR
  PE_OK
  PE_ERROR
);

extends 'Lemonldap::NG::Portal::Lib::LDAP';

our $VERSION = '2.1.0';

# RUNNING METHODS

# PRIVATE METHODS
sub modifCertificate {

    my $self         = shift;
    my $newcertif    = shift;
    my $usercertif   = shift;
    my $req          = shift;
    my $ceaAttribute = $self->conf->{certificateResetByMailCeaAttribute}
      || "description";
    my $certificateAttribute =
      $self->conf->{certificateResetByMailCertificateAttribute}
      || "userCertificate;binary";

    # Set the dn unless done before
    #
    #
    my $dn;
    if ( $req->userData->{_dn} ) {
        $dn = $req->userData->{_dn};
        $self->logger->debug("Get DN from request data: $dn");
    }
    else {
        $dn = $req->sessionInfo->{_dn};
        $self->logger->debug("Get DN from session data: $dn");
    }
    unless ($dn) {
        $self->logger->error('"dn" is not set, aborting password modification');
        return PE_ERROR;
    }

    #my $dn = "uid=" . $req->{user}. "," . $self->conf->{ldapBase};

    my $result = $self->ldap->modify(
        $dn,
        replace => [
            $ceaAttribute           => $newcertif,
            "$certificateAttribute" => [$usercertif]
        ]
    );

    unless ( $result->code == 0 ) {
        $self->logger->debug( "LDAP modify Error: " . $result->code );
        $self->ldap->unbind;
        $self->{flags}->{ldapActive} = 0;
        $self->ldap->unbind;
        return PE_LDAPERROR;
    }

    $self->logger->debug("$ceaAttribute set to $newcertif");

    return PE_OK;

}

1;
